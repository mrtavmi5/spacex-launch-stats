import { gql, useQuery } from "@apollo/client";
import React from "react";

const LAUNCHES_QUERY = gql`
  query LaunchesQuery {
    launches {
      flight_number
      mission_name
      launch_date_local
      launch_success
    }
  }
`;

export const Launches = () => {
  const { loading, error, data } = useQuery(LAUNCHES_QUERY);

  if (error) console.log(error);
  console.log(data);
  return (
    <div>
      <h1 className="display-4 my-3">Launches</h1>
      {loading && <h4>Loading...</h4>}
      {!loading && <h1>test</h1>}
    </div>
  );
};

export default Launches;
